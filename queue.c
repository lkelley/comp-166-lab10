//
// Created by Landon on 2020-01-21.
//

#include <stdlib.h>

#include "queue.h"


/**
 * Adds a new node with specified data to the end of the queue
 *
 * @param queue Queue to add the new node to
 * @param newItem New item for the new node's data
 * @return The data in the new node
 */
ItemType *enqueue(Queue *queue, const ItemType *newItem) {
    /*
     * Initialize new node, malloc, and copy data from newItem to the new node's data
     */
    ListNode *newNode = (ListNode *)malloc(sizeof(ListNode));
    newNode->next = (ListNode *)malloc(sizeof(ListNode));
    newNode->data = (ItemType *)malloc(sizeof(ItemType));
    *newNode->data = *newItem; // Data copy, not reference

    /*
     * Set front and rear nodes if the queue is empty
     */
    if (queue->size == 0) {
        queue->front = newNode;
        queue->rear = newNode;
    }
    /*
     * Set next on current queue's rear and set new queue rear to the new node
     */
    else {
        queue->rear->next = newNode;
        queue->rear = newNode;
    }

    // Increment queue size
    queue->size++;

    return newNode->data;
}


/**
 * Removes the front node and returns its data
 *
 * @param queue Queue to dequeue from
 * @return The data from the removed node
 */
ItemType *dequeue(Queue *queue) {
    // Empty queue
    if (queue->size == 0) return NULL;

    // Get pointer to current front
    ListNode *deletedNode = queue->front;

    /*
     * Set front of queue to next node
     */
    if (queue->size > 1) {
        queue->front = queue->front->next;
    }

    // Get return data
    ItemType *data = deletedNode->data;

    // Deallocate the deleted node
    free(deletedNode);

    // Decrement size
    queue->size--;

    return data;
}


/**
 * Get the current size of the queue
 * @param queue Queue to size
 * @return The current size of the queue
 */
int queueSize(const Queue queue) {
    return queue.size;
}


/**
 * Prints the elements of the queue, dequeueing them
 * @param queue Queue to print
 * @param stream Output stream to print to
 */
void printQueue(Queue queue, FILE *stream) {
    while (queue.size > 0) {
        ItemType *item = dequeue(&queue);

        if (item != NULL) {
            fprintf(stream, ITEM_FORMAT "\n", *item);
        }
    }
}
