//
// Created by Landon on 2020-01-21.
//

#ifndef LAB_10___QUEUE_LINKED_LIST_QUEUE_H
#define LAB_10___QUEUE_LINKED_LIST_QUEUE_H

#include <stdio.h>

// The following 3 lines must all be in agreement
typedef int ItemType;
#define ITEM_PROMPT "an integer"
#define ITEM_FORMAT "%d"

// A node for a singly-linked list
typedef struct listNode {
    struct listNode *next;
    ItemType *data;
} ListNode;

// A queue data structure
typedef struct {
    ListNode *front;
    ListNode *rear;
    int size; // Number of items currently in the queue
} Queue;

/* Add an item to the rear of the dynamically-allocated queue.
 * Returns:  pointer to the item if successful, NULL if not */
ItemType *enqueue (Queue *queue, const ItemType *newItem);

/* Removes an item from the front of the queue.
 * Returns:  pointer to the item if successful, NULL if empty */
ItemType *dequeue (Queue *queue);

/* Returns:  number of items in the queue */
int queueSize (Queue queue);

/* Print the contents of the queue to the specified stream */
void printQueue (Queue queue, FILE *stream);

#endif //LAB_10___QUEUE_LINKED_LIST_QUEUE_H
